import React from 'react'
import { OpenCvProvider } from 'opencv-react'
import Canvas from '../lib/Canvas'
import T from 'prop-types'

const PolygonCanvas = React.forwardRef((props, ref) => {
  if (!props.image) {
    return null
  }

  return (
    <OpenCvProvider openCvPath={props.openCvPath}>
      <Canvas {...props} />
    </OpenCvProvider>
  )
})

export default PolygonCanvas

PolygonCanvas.propTypes = {
  openCvPath: T.string
}
