import React, { useCallback } from 'react'
import Draggable from 'react-draggable'
import T from 'prop-types'

const buildPointStyle = (pointStyle) => ({
  width: pointStyle.size,
  height: pointStyle.size,
  ...pointStyle,
  borderRadius: '100%',
  position: 'absolute',
  zIndex: 1001
})

const Point = ({
  points,
  pointArea,
  defaultPosition,
  pointStyle ,
  onStop: externalOnStop,
  onDrag: externalOnDrag,
  bounds
}) => {
  const onDrag = useCallback(
    (_, position) => {
      externalOnDrag(
        {
          ...position,
          x: position.x + pointStyle.size / 2,
          y: position.y + pointStyle.size / 2
        },
        pointArea
      )
    },
    [externalOnDrag]
  )

  const onStop = useCallback(
    (_, position) => {
      externalOnStop(
        {
          ...position,
          x: position.x + pointStyle.size / 2,
          y: position.y + pointStyle.size / 2
        },
        pointArea,
        points
      )
    },
    [externalOnDrag, points]
  )

  // console.log('Points', points)
  // console.log('pointArea', pointArea)

  return (
    <Draggable
      bounds={bounds}
      defaultPosition={defaultPosition}
      position={{
        x: points[pointArea].x - pointStyle.size / 2,
        y: points[pointArea].y - pointStyle.size / 2
      }}
      onDrag={onDrag}
      onStop={onStop}
    >
      <div style={buildPointStyle({ ...pointStyle })} />
    </Draggable>
  )
}

export default Point

Point.propTypes = {
  points: T.shape({
    'left-top': T.shape({ x: T.number, y: T.number }).isRequired,
    'right-top': T.shape({ x: T.number, y: T.number }).isRequired,
    'right-bottom': T.shape({ x: T.number, y: T.number }).isRequired,
    'left-bottom': T.shape({ x: T.number, y: T.number }).isRequired
  }),
  pointArea: T.oneOf(['left-top', 'right-top', 'right-bottom', 'left-bottom']),
  defaultPosition: T.shape({
    x: T.number,
    y: T.number
  }),
  pointStyle: T.shape({
    size: T.number,
    backgroundColor: T.string,
    border: T.string
  }),
  onStop: T.func,
  onDrag: T.func
}
