import React, { Fragment } from 'react'
import Point from './Point'
import T from 'prop-types'

const Points = (props) => {
  const { previewDims, ...otherProps } = props
  return (
    <>
      <Point
        pointArea='left-top'
        defaultPosition={{ x: 0, y: 0 }}
        {...otherProps}
      />
      <Point
        pointArea='right-top'
        defaultPosition={{ x: previewDims.width, y: 0 }}
        {...otherProps}
      />
      <Point
        pointArea='right-bottom'
        defaultPosition={{ x: 0, y: previewDims.height }}
        {...otherProps}
      />
      <Point
        pointArea='left-bottom'
        defaultPosition={{
          x: previewDims.width,
          y: previewDims.height
        }}
        {...otherProps}
      />
    </>
  )
}

export default Points

Points.propTypes = {
  previewDims: T.shape({
    ratio: T.number,
    width: T.number,
    height: T.number
  })
}
