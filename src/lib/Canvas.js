import React, {
  useCallback,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
  Fragment
} from 'react'
import { useOpenCv } from 'opencv-react'
import T from 'prop-types'

import { calcDims, readFile, isInside } from '../lib/utils'
import Points from './Points'
import { applyFilter, transform } from '../lib/imgManipulation'
import PointsDelimiters from './PointsDelimiters'
import { MODE } from './constant'

let imageResizeRatio
let imageWidthRatio, imageHeighRatio

const Canvas = ({
  image,
  onChange,
  lineStyle = {
    lineWidth: 5,
    strokeStyle: '#85fa75',
    fillStyle: '#ED1C24'
  },
  pointStyle = {
    size: 50,
    backgroundColor: '#3cabe2',
    border: '#3cabe2'
  },
  setPoints,
  setResultImage,
  points,
  maxWidth,
  maxHeight,
  showPoint = true,
  isMobile
}) => {
  const { loaded: cvLoaded, cv } = useOpenCv()
  const canvasRef = useRef()
  const previewCanvasRef = useRef()
  const [previewDims, setPreviewDims] = useState({
    width: 0,
    height: 0
  })
  const [canMoveFrame, setCanMoveFrame] = useState(false)
  const [artPoints, setArtPoints] = useState()
  const [mode, setMode] = useState(MODE.PREVIEW)
  const [loading, setLoading] = useState(false)
  // const [mode, setMode] = useState('crop')

  // useEffect(() => {
  //   if (mode === 'preview') {
  //     showPreview()
  //   }
  // }, [mode])

  const createCanvas = (src) => {
    return new Promise((resolve, reject) => {
      const img = document.createElement('img')
      img.onload = async () => {
        canvasRef.current = document.createElement('canvas')
        canvasRef.current.width = img.width
        canvasRef.current.height = img.height
        setPreviewDims({ height: img.height, width: img.width })
        if (!points && !artPoints) {
          setArtPoints({
            'left-top': { x: 0, y: 0 },
            'right-top': { x: img.width, y: 0 },
            'right-bottom': {
              x: img.width,
              y: img.height
            },
            'left-bottom': { x: 0, y: img.height },
            loading: true
          })
        }
        const ctx = canvasRef.current.getContext('2d')
        ctx.drawImage(img, 0, 0)
        setPreviewPaneDimensions()
        resolve()
      }
      img.src = src
    })
  }
  const setPreviewPaneDimensions = () => {
    // set preview pane dimensions
    const newPreviewDims = calcDims(
      canvasRef.current.width,
      canvasRef.current.height,
      maxWidth,
      maxHeight
    )
    setPreviewDims(newPreviewDims)

    previewCanvasRef.current.width = newPreviewDims.width
    previewCanvasRef.current.height = newPreviewDims.height

    // previewCanvasRef.current.width = canvasRef.current.width
    // previewCanvasRef.current.height = canvasRef.current.height

    imageResizeRatio = newPreviewDims.width / canvasRef.current.width
    imageWidthRatio = canvasRef.current.width / newPreviewDims.width
    imageHeighRatio = canvasRef.current.height / newPreviewDims.height
  }

  const showPreview = (image) => {
    const src = image || cv.imread(canvasRef.current)
    const dst = new cv.Mat()
    const dsize = new cv.Size(0, 0)
    cv.resize(
      src,
      dst,
      dsize,
      imageResizeRatio,
      imageResizeRatio,
      cv.INTER_AREA
    )
    cv.imshow(previewCanvasRef.current, dst)
    src.delete()
    dst.delete()
  }

  useEffect(() => {
    const bootstrap = async () => {
      const src = await readFile(image)
      await createCanvas(src)
      showPreview()
      if (points) {
        const resultPoint = {}
        let newImageResizeRatio = isMobile ? 1 : imageResizeRatio

        resultPoint['left-top'] = {
          x: points['left-top'].x * newImageResizeRatio,
          y: points['left-top'].y * newImageResizeRatio
        }
        resultPoint['right-top'] = {
          x: points['right-top'].x * newImageResizeRatio,
          y: points['right-top'].y * newImageResizeRatio
        }
        resultPoint['right-bottom'] = {
          x: points['right-bottom'].x * newImageResizeRatio,
          y: points['right-bottom'].y * newImageResizeRatio
        }
        resultPoint['left-bottom'] = {
          x: points['left-bottom'].x * newImageResizeRatio,
          y: points['left-bottom'].y * newImageResizeRatio
        }
        // const resultPoint = setPointDimention(points, true)
        setArtPoints(resultPoint)
      }
      setLoading(false)
    }

    if (image && previewCanvasRef.current && cvLoaded) {
      bootstrap()
    } else {
      setLoading(true)
    }
  }, [image, previewCanvasRef.current, cvLoaded])

  useEffect(() => {
    if (onChange && artPoints) {
      const resultPoint = {}
      resultPoint['left-top'] = {
        x: artPoints['left-top'].x * imageWidthRatio,
        y: artPoints['left-top'].y * imageHeighRatio
      }
      resultPoint['right-top'] = {
        x: artPoints['right-top'].x * imageWidthRatio,
        y: artPoints['right-top'].y * imageHeighRatio
      }
      resultPoint['right-bottom'] = {
        x: artPoints['right-bottom'].x * imageWidthRatio,
        y: artPoints['right-bottom'].y * imageHeighRatio
      }
      resultPoint['left-bottom'] = {
        x: artPoints['left-bottom'].x * imageWidthRatio,
        y: artPoints['left-bottom'].y * imageHeighRatio
      }
      onChange({ ...resultPoint, loading })
    }
  }, [artPoints, loading])

  const onDrag = useCallback((position, area) => {
    const { x, y } = position
    const canvas = previewCanvasRef.current
    const context = canvas.getContext('2d')
    setArtPoints((cPs) => ({ ...cPs, [area]: { x, y } }))
  }, [])

  const onStop = useCallback((position, area, cropPoints) => {
    const { x, y } = position
    // clearMagnifier();
    setArtPoints((cPs) => ({ ...cPs, [area]: { x, y } }))
    const resultImage = previewCanvasRef.current.toDataURL()
    if (setResultImage) {
      setResultImage(resultImage)
    }
    // if (onDragStop) {
    //   onDragStop({ ...cropPoints, [area]: { x, y } });
    // }
  }, [])

  const onMouseDown = (e, pointMouse) => {
    // set canMoveFrame after set mouse up
  }
  const onMouseUp = (e, pointMouse) => {
    // console.log('onMouseUp', e)
    // check point in
  }
  const onMouseMove = (e, pointMouse) => {
    if (isInside(artPoints, pointMouse)) {
      // console.log('isInside')
    }
    // console.log('onMouseMove', pointMouse)
  }

  const handleCanvasClick = (e) => {
    mode === MODE.PREVIEW ? setMode(MODE.EDIT) : setMode(MODE.PREVIEW)
  }

  return (
    <div style={{ position: 'relative' }}>
      {previewDims && artPoints && showPoint && (
        <Fragment>
          {mode === MODE.EDIT && (
            <Points
              previewDims={previewDims}
              points={artPoints}
              pointStyle={pointStyle}
              onDrag={onDrag}
              onStop={onStop}
              bounds={{
                left:
                  previewCanvasRef?.current?.offsetLeft - pointStyle.size / 2,
                top: previewCanvasRef?.current?.offsetTop - pointStyle.size / 2,
                right:
                  previewCanvasRef?.current?.offsetLeft -
                  pointStyle.size / 2 +
                  previewCanvasRef?.current?.offsetWidth,
                bottom:
                  previewCanvasRef?.current?.offsetTop -
                  pointStyle.size / 2 +
                  previewCanvasRef?.current?.offsetHeight
              }}
            />
          )}
          {/* rectangle canvas */}
          <PointsDelimiters
            previewDims={previewDims}
            points={artPoints}
            lineStyle={lineStyle}
            handleCanvasClick={handleCanvasClick}
            onMouseDown={onMouseDown}
            onMouseUp={onMouseUp}
            onMouseMove={onMouseMove}
          />
          {/* image */}
        </Fragment>
      )}
      <canvas
        width={previewDims.width}
        height={previewDims.height}
        ref={previewCanvasRef}
      />
    </div>
  )
}

export default Canvas

Canvas.propTypes = {
  image: T.object.isRequired,
  onDragStop: T.func,
  onChange: T.func,
  canvasRef: T.shape({
    current: T.shape({
      done: T.func.isRequired,
      backToCrop: T.func.isRequired
    })
  }),
  lineWidth: T.number,
  pointBgColor: T.string,
  pointBorder: T.string,
  lineColor: T.string
}
