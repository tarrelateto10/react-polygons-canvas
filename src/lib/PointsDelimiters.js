import React, { useCallback, useEffect, useRef } from 'react'
import T from 'prop-types'
const PointsDelimiters = ({
  points,
  previewDims,
  lineStyle,
  handleCanvasClick,
  onMouseDown: externalOnMouseDown,
  onMouseUp: externalOnMouseUp,
  onMouseMove: externalOnMouseMove
}) => {
  const canvas = useRef()

  const clearCanvas = useCallback(() => {
    const ctx = canvas.current.getContext('2d')
    ctx.clearRect(0, 0, previewDims.width, previewDims.height)
  }, [canvas.current, previewDims])

  const sortPoints = useCallback(() => {
    const sortOrder = ['left-top', 'right-top', 'right-bottom', 'left-bottom']
    return sortOrder.reduce((acc, pointPos) => [...acc, points[pointPos]], [])
  }, [points])

  const drawShape = useCallback(
    ([point1, point2, point3, point4]) => {
      const ctx = canvas.current.getContext('2d')
      ctx.lineWidth = lineStyle.lineWidth
      ctx.strokeStyle = lineStyle.strokeStyle
      ctx.fillStyle = lineStyle.fillStyle
      ctx.beginPath()

      //line
      // ctx.moveTo(point1.x + pointSize / 2, point1.y)
      // ctx.lineTo(point2.x - pointSize / 2, point2.y)

      // ctx.moveTo(point2.x, point2.y + pointSize / 2)
      // ctx.lineTo(point3.x, point3.y - pointSize / 2)

      // ctx.moveTo(point3.x - pointSize / 2, point3.y)
      // ctx.lineTo(point4.x + pointSize / 2, point4.y)

      // ctx.moveTo(point4.x, point4.y - pointSize / 2)
      // ctx.lineTo(point1.x, point1.y + pointSize / 2)

      ctx.moveTo(point1.x, point1.y)
      ctx.lineTo(point2.x, point2.y)
      ctx.lineTo(point3.x, point3.y)
      ctx.lineTo(point4.x, point4.y)

      ctx.closePath()
      ctx.stroke()
      ctx.fill()
    },
    [canvas.current]
  )

  useEffect(() => {
    if (points) {
      clearCanvas()
      const sortedPoints = sortPoints()
      drawShape(sortedPoints)
    }
  }, [points, canvas.current])

  const clickOnImage = (e) => {
    handleCanvasClick(e)
  }

  const onMouseDown = (e) => {
    var rect = canvas.current.getBoundingClientRect()
    const pointMouse = { x: e.clientX - rect.left, y: e.clientY - rect.top }
    externalOnMouseDown(e, pointMouse)
  }

  const onMouseUp = (e) => {
    var rect = canvas.current.getBoundingClientRect()
    const pointMouse = { x: e.clientX - rect.left, y: e.clientY - rect.top }
    externalOnMouseUp(e, pointMouse)
  }
  const onMouseMove = (e) => {
    var rect = canvas.current.getBoundingClientRect()
    const pointMouse = { x: e.clientX - rect.left, y: e.clientY - rect.top }
    externalOnMouseMove(e, pointMouse)
  }
  return (
    <canvas
      ref={canvas}
      style={{
        position: 'absolute',
        zIndex: 5
      }}
      onClick={(e) => clickOnImage(e)}
      width={previewDims.width}
      height={previewDims.height}
      onMouseDown={(e) => onMouseDown(e)}
      onMouseMove={(e) => onMouseMove(e)}
      onMouseUp={(e) => onMouseUp(e)}
    />
  )
}

export default PointsDelimiters

PointsDelimiters.propTypes = {
  previewDims: T.shape({
    ratio: T.number,
    width: T.number,
    height: T.number
  }),
  points: T.shape({
    'left-top': T.shape({ x: T.number, y: T.number }).isRequired,
    'right-top': T.shape({ x: T.number, y: T.number }).isRequired,
    'right-bottom': T.shape({ x: T.number, y: T.number }).isRequired,
    'left-bottom': T.shape({ x: T.number, y: T.number }).isRequired
  }),
  lineColor: T.string,
  lineWidth: T.number,
  pointSize: T.number
}
