export const readFile = (file) => {
  if (file instanceof File) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader()
      reader.onload = (event) => {
        resolve(reader.result)
      }
      reader.onerror = (err) => {
        reject(err)
      }
      reader.readAsDataURL(file)
    })
  }
  if (typeof file === 'string') {
    return Promise.resolve(file)
  }
}

export const calcDims = (
  width,
  height,
  externalMaxWidth,
  externalMaxHeight
) => {
  const ratio = width / height

  const maxWidth = externalMaxWidth || window.innerWidth
  const maxHeight = externalMaxHeight || window.innerHeight
  const calculated = {
    width: maxWidth,
    height: Math.round(maxWidth / ratio),
    ratio: ratio
  }

  if (calculated.height > maxHeight) {
    calculated.height = maxHeight
    calculated.width = Math.round(maxHeight * ratio)
  }
  return calculated
}
const INF = 10000

const onSegment = (p, q, r) => {
  if (
    q.x <= Math.max(p.x, r.x) &&
    q.x >= Math.min(p.x, r.x) &&
    q.y <= Math.max(p.y, r.y) &&
    q.y >= Math.min(p.y, r.y)
  ) {
    return true
  }
  return false
}

const isOrientation = (p, q, r) => {
  let val = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y)

  if (val == 0) {
    return 0 // collinear
  }
  return val > 0 ? 1 : 2 // clock or counterclock wise
}

const doIntersect = (p1, q1, p2, q2) => {
  // Find the four orientations needed for
  // general and special cases
  let o1 = isOrientation(p1, q1, p2)
  let o2 = isOrientation(p1, q1, q2)
  let o3 = isOrientation(p2, q2, p1)
  let o4 = isOrientation(p2, q2, q1)

  // General case
  if (o1 != o2 && o3 != o4) {
    return true
  }

  // Special Cases
  // p1, q1 and p2 are collinear and
  // p2 lies on segment p1q1
  if (o1 == 0 && onSegment(p1, p2, q1)) {
    return true
  }

  // p1, q1 and p2 are collinear and
  // q2 lies on segment p1q1
  if (o2 == 0 && onSegment(p1, q2, q1)) {
    return true
  }

  // p2, q2 and p1 are collinear and
  // p1 lies on segment p2q2
  if (o3 == 0 && onSegment(p2, p1, q2)) {
    return true
  }

  // p2, q2 and q1 are collinear and
  // q1 lies on segment p2q2
  if (o4 == 0 && onSegment(p2, q1, q2)) {
    return true
  }

  // Doesn't fall in any of the above cases
  return false
}

export const isInside = (_polygon, point) => {
  let polygon = [
    _polygon['left-top'],
    _polygon['right-top'],
    _polygon['right-bottom'],
    _polygon['left-bottom']
  ]

  let total_point = 4

  let extreme = { x: INF, y: point.y }

  let count = 0,
    i = 0
  do {
    let next = (i + 1) % total_point

    // Check if the line segment from 'p' to
    // 'extreme' intersects with the line
    // segment from 'polygon[i]' to 'polygon[next]'
    if (doIntersect(polygon[i], polygon[next], point, extreme)) {
      // If the point 'p' is colinear with line
      // segment 'i-next', then check if it lies
      // on segment. If it lies, return true, otherwise false
      if (isOrientation(polygon[i], point, polygon[next]) == 0) {
        return onSegment(polygon[i], point, polygon[next])
      }

      count++
    }
    i = next
  } while (i != 0)

  // Return true if count is odd, false otherwise
  return count % 2 == 1
}
