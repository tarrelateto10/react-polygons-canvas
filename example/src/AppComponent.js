import React, { useCallback } from 'react'
import './App.css'

import PolygonCanvas from 'react-polygons-canvas'
import { Button, Spin, Upload } from 'antd'

import './App-Component.css'

export class App extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      points: {
        'left-top': { x: 126, y: 188 },
        'right-top': { x: 257, y: 196 },
        'right-bottom': {
          x: 250,
          y: 226
        },
        'left-bottom': { x: 119, y: 219 },
        loading: true
      },
      img: null,
      resultImage: null
    }
  }

  onImgSelection = async (e) => {
    if (e.target.files && e.target.files.length > 0) {
      // it can also be a http or base64 string for example
      this.setState({ img: e.target.files[0] })
    }
  }

  showPoints = () => {
    console.log(this.state.points)
  }
  showResultImage = () => {
    console.log(this.state.resultImage)
  }
  onChange = (s) => this.setState({ points: s })

  render() {
    // console.log(this.state)

    return (
      <div>
        <div>test Component</div>

        <div>
          <input
            type='file'
            onChange={this.onImgSelection.bind()}
            accept='image/*'
          />
        </div>
        <div className='content-container'>
          {this.state.img && (
            <>
              <PolygonCanvas
                points={this.state.points}
                maxWidth={650}
                image={this.state.img}
                lineStyle={{
                  lineWidth: 5,
                  strokeStyle: '#85fa75',
                  fillStyle: '#ffffff'
                }}
                pointStyle={{
                  size: 10,
                  backgroundColor: '#3cabe2',
                  border: '4px solid #3cabe2'
                }}
                onChange={this.onChange}
              />
              {this.state.points.loading && <Spin />}
            </>
          )}
        </div>

        <button onClick={this.showPoints.bind()}>Points</button>
        <br />
        <button onClick={this.showResultImage.bind()}>Image</button>
      </div>
    )
  }
}
export default App
