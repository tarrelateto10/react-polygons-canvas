import './index.css'

import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
// import App from './AppComponent'

ReactDOM.render(<App />, document.getElementById('root'))
