import React, { useRef, useState, useCallback } from 'react'

import { Button, Spin, Upload } from 'antd'

import PolygonCanvas from 'react-polygons-canvas'

import './App.css'

const App = () => {
  const [img, setImg] = useState()
  // const canvasRef = useRef()
  const [pointState, setPointState] = useState({
    'left-top': { x: 126, y: 188 },
    'right-top': { x: 257, y: 196 },
    'right-bottom': {
      x: 250,
      y: 226
    },
    'left-bottom': { x: 119, y: 219 }
  })

  const onChange = useCallback((s) => setPointState(s), [])

  const onImgSelection = async (e) => {
    if (e.target.files && e.target.files.length > 0) {
      // it can also be a http or base64 string for example
      setImg(e.target.files[0])
    }
  }

  const showPoints = () => {
    console.log(pointState)
  }

  return (
    <div>
      <div>test</div>

      <div>
        <input type='file' onChange={onImgSelection} accept='image/*' />
      </div>
      <div className='content-container'>
        {img && (
          <>
            <PolygonCanvas
              points={pointState}
              image={img}
              lineStyle={{
                lineWidth: 5,
                strokeStyle: '#85fa75',
                fillStyle: '#ffffff'
              }}
              pointStyle={{
                size: 10,
                backgroundColor: '#3cabe2',
                border: '4px solid #3cabe2'
              }}
              onChange={onChange}
              showPoint
              maxWidth={650}
            />
            {pointState?.loading && <Spin />}
          </>
        )}
      </div>

      <button onClick={showPoints}>Points</button>
      {JSON.stringify(pointState)}
    </div>
  )
}

export default App
